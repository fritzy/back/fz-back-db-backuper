---
- changed_when: false
  name: 'find {{ secrets_container }} name'
  register: secret_container_name_result
  args:
    executable: /bin/bash
  shell: |
    set -o pipefail && \
    docker ps | grep "{{ secrets_container }}"

- name: 'extract {{ secrets_container }} task'
  set_fact:
    secrets_container_name: "{{ secret_container_name_result.stdout | regex_replace('^.*(' + secrets_container + '.*)$', '\\1') }}"

- changed_when: false
  name: login to vault
  register: vault_login
  command: "docker exec {{ secrets_container_name }} vault login token={{ secret_token }}"

- name: renew secret vault token
  command: "docker exec {{ secrets_container_name }} vault token renew {{ secret_token }}"

- name: fetch db credentials
  register: db_credentials
  command: 'docker exec {{ secrets_container_name }} vault read database/creds/fritzy-db-ro'

- changed_when: false
  name: 'find {{ db_container }} name'
  register: db_container_name_result
  args:
    executable: /bin/bash
  shell: |
    set -o pipefail && \
    docker ps | grep "{{ db_container }}"

- changed_when: false
  name: set dump variables
  ansible.builtin.set_fact:
    db_container_name: "{{ db_container_name_result.stdout | regex_replace('^.*(' + db_container + '.*)$', '\\1') }}"
    db_username: "{{ db_credentials.stdout_lines | select('search', '^.*username.*$') | list | map('regex_replace', '^username\\s*(.*)$', '\\1') | list | first }}"
    db_password: "{{ db_credentials.stdout_lines | select('search', '^.*password.*$') | list | map('regex_replace', '^password\\s*(.*)$', '\\1') | list | first }}"
    dump_time: "{{ ansible_date_time.iso8601_basic_short }}"

- name: create database dump
  ansible.builtin.shell: "docker exec {{ db_container_name }} sh -c 'mysqldump --single-transaction --skip-lock-tables --databases fritzy -u{{ db_username }} -p{{ db_password }}' > fritzy.{{ dump_time }}.sql"

- name: compress database dump
  community.general.archive:
    path: "fritzy.{{ dump_time }}.sql"
    dest: "fritzy.{{ dump_time }}.sql.gz"

- name: remove database dump
  ansible.builtin.file:
    path: "fritzy.{{ dump_time }}.sql"
    state: absent

- name: encrypt database dump
  ansible.builtin.command: "gpg --output fritzy.{{ dump_time }}.sql.gz.gpg --encrypt --recipient {{ dump_recipient }} fritzy.{{ dump_time }}.sql.gz"

- name: remove database dump archive
  ansible.builtin.file:
    path: "fritzy.{{ dump_time }}.sql.gz"
    state: absent

- name: copy dump to rclone remote
  ansible.builtin.command: "rclone copy fritzy.{{ dump_time }}.sql.gz.gpg {{ rclone_remote }}:/dumps"

- name: remove database dump crypted archive
  ansible.builtin.file:
    path: "fritzy.{{ dump_time }}.sql.gz.gpg"
    state: absent
