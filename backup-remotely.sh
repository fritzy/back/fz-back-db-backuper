#!/bin/bash
source backup-remotely-env.sh

docker run --rm -it \
  -e DB_CONTAINER=$DB_CONTAINER \
  -e DUMP_RECIPIENT=$DUMP_RECIPIENT \
  -e RCLONE_REMOTE=$RCLONE_REMOTE \
  -e SECRET_TOKEN=$SECRET_TOKEN \
  -e SECRETS_CONTAINER=$SECRETS_CONTAINER \
  -v $(pwd):/ansible \
  -v ~/.ssh/id_rsa_gitlab_ci:/root/.ansible/private_key \
  willhallonline/ansible:2.10-alpine-3.11 \
  ansible-playbook -i inventories/staging backup.yml
