# Fritzy back database backuper

Backup the database encrpted by a gpg key to a dumps folder of a rclone remote.

## Environment variables

- DB_CONTAINER: **database container**
- RCLONE_REMOTE: **rclone remote to copy dumps to**
- DUMP_RECIPIENT: **dump gpg key recipient**
- SECRET_TOKEN: **vault secret token used to fetch db credentials**
- SECRETS_CONTAINER: **vault container**

## Things done

```plantuml
@startuml
hide empty description
[*] --> fetch_db_credentials
fetch_db_credentials --> create_dump
create_dump --> archive_dump
archive_dump --> encrypt_dump
encrypt_dump --> upload_dump
upload_dump --> [*]

state "Fetch database credentials from vault" as fetch_db_credentials
state "Create dump" as create_dump
state "Gzip the dump" as archive_dump
state "Encrypt the dump with gpg" as encrypt_dump
state "Upload the dump to a rclone remote" as upload_dump

@enduml
```
